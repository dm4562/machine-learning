import matplotlib.pyplot as plt

plt.style.use('ggplot')


rhc_accuracy = [
    75.824, 75.943, 76.215, 75.591, 76.328, 75.525, 78.931, 76.448, 78.772, 78.201, 78.115, 77.477, 78.021, 78.712
]

rhc_time = [
    15.411, 30.148, 44.653, 60.727, 74.712, 89.938, 105.046, 120.482, 134.861, 150.277, 164.083, 174.968, 196.228, 204.885
]

sa_accuracy = [
    75.423, 74.465, 75.797, 75.432, 24.568, 75.432, 75.432, 75.432, 77.882, 77.689, 77.055, 76.541, 76.109, 78.758
]

sa_time = [
    15.071, 30.218, 45.282, 60.742, 75.327, 89.547, 105.035, 120.825, 135.603, 151.406, 165.995, 181.465, 195.506, 212.287
]

ga_accuracy = [
    76.586, 77.976, 75.432, 75.432, 77.756, 75.657, 78.426, 78.705, 78.685, 76.036, 78.041, 78.752, 78.692, 78.725
]

ga_time = [
    609.775, 1236.093, 1852.076, 2493.339, 3075.779, 3708.950, 4338.483, 4838.220, 5552.812,
    6174.963, 6834.195, 7500.575, 8108.432, 8057.064
]

backprop_accuracy = [78.645418326693232, 78.293492695883131, 79.10358565737053, 77.231075697211153, 77.901726427622847, 77.104913678618857, 78.897742363877825,
                     78.326693227091639, 79.442231075697208, 75.710491367861891, 79.687915006640111, 77.901726427622847, 77.881806108897749, 78.778220451527233]

backprop_time = [2.6130806640721858, 5.1128671378828585, 3.445878154132515, 2.04538109106943, 1.6049761220347136, 1.609702476998791, 4.366837769979611,
                 1.5233449570368975, 6.383662099018693, 3.926659391960129, 4.1722117678727955, 4.7181167509406805, 6.142463076859713, 0.9469002259429544]

iterations = [i for i in range(100, 1401, 100)]
fig = plt.figure()
plt.plot(iterations, rhc_accuracy, label='Random Hill Climbing')
plt.plot(iterations, sa_accuracy, label="Simulated Annealing")
plt.plot(iterations, ga_accuracy, label="Genetic Algorithm")
plt.plot(iterations, backprop_accuracy, label="Backpropagation")
plt.xlabel("Number of Iterations")
plt.ylabel("Accuracy")
plt.legend(loc="best")
plt.suptitle("Accuracy of Various Algorithms")
fig.savefig('graphs/accuracy_graph.png')

fig = plt.figure()
plt.plot(iterations, rhc_time, label='Random Hill Climbing')
plt.plot(iterations, sa_time, label="Simulated Annealing")
plt.plot(iterations, ga_time, label="Genetic Algorithm")
plt.plot(iterations, backprop_time, label="Backpropagation")
plt.xlabel("Number of Iterations")
plt.ylabel("Time Taken")
plt.legend(loc="best")
plt.suptitle("Time vs Iterations")
fig.savefig('graphs/time_graph.png')
