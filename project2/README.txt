Location of all the output files:
output/

Location of all the graphs:
graphs/

Datasets for part 1:
data_sets/

Code for part 1:
----------------
 *  To generate outputs
    $ cd ABAGAIL
    $ ant
    $ java -cp ABAGAIL.jar opt.test.RandomizedOptimization [iterations]

 *  To generate graphs
    $ python3 part1_graphs.py

Code for part 2:
----------------
 *  To generate outputs
    - Problem 1 (Traveling Salesman)    :   $ java -cp ABAGAIL.jar opt.test.TravelingSalesmanTest [N]
    - Problem 2 (Flip Flop)             :   $ java -cp ABAGAIL.jar opt.test.FlipFlopTest [N]
    - Problem 3 (Four Peaks)            :   $ java -cp ABAGAIL.jar opt.test.FourPeaksTest [iterations]

 *  To generate graphs
    $ python3 part2_graphs.py

