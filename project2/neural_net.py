import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from timeit import default_timer as timer

train = pd.read_csv(
    "../data_sets/census/census-income-train-clean.txt", header=None)
test = pd.read_csv(
    "../data_sets/census/census-income-test-clean.txt", header=None)

train_x = train[[x for x in train.columns if x != 14]]
train_y = train[14]

test_x = test[[x for x in test.columns if x != 14]]
test_y = test[14]

testing_accuracy, training_accuracy, train_time, test_time = [], [], [], []


for i in range(100, 1501, 100):
    start = timer()

    clf = MLPClassifier(hidden_layer_sizes=(8, 8, 8, 8, 8, 8), max_iter=i)
    clf.fit(train_x, train_y)

    y = clf.predict(train_x)
    train_end = timer()

    y_ = clf.predict(test_x)
    test_end = timer()

    tr_a = 100 * accuracy_score(train_y, y)
    te_a = 100 * accuracy_score(test_y, y_)
    tr_t = train_end - start
    te_t = test_end - start

    print("Iterations: {} \nTraining Accuracy: {} \nTesting Accuracy: {} \nTraining Time: {} \nTesting Time: {}\n\n".format(
        i, tr_a, te_a, tr_t, te_t))

    testing_accuracy.append(te_a)
    training_accuracy.append(tr_a)
    # print(accuracy_score(test_y, y_))
    train_time.append(tr_t)
    test_time.append(te_t)
    # print(train_end - start)

print(training_accuracy)
print(testing_accuracy)
print(train_time)
print(test_time)
