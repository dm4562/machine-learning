package testing;

import shared.FixedIterationTrainer;
import shared.DataSet;
import shared.Instance;
import shared.SumOfSquaresError;
import func.nn.backprop.BackPropagationNetwork;
import func.nn.backprop.BackPropagationNetworkFactory;
import func.nn.backprop.BatchBackPropagationTrainer;
import func.nn.backprop.RPROPUpdateRule;

import java.util.*;
import java.io.*;
import java.text.*;

/**
 * Backpropagation without randomized optimization
 * @author Christopher Tam
 * @version 1.0
 */
public class NormalBackPropTest {   
    private static final int TRAINING_SET_SIZE = 30162;
    private static final int TESTING_SET_SIZE = 15060;
    private static final int ATTRIBUTE_COUNT = 14;
    
    private static final int INPUT_LAYER = ATTRIBUTE_COUNT;
    private static final int HIDDEN_LAYERS = 6;
    private static final int OUTPUT_LAYER = 1;

    private static final DecimalFormat df = new DecimalFormat("0.000");
    
    private static final String TRAINING_FILE = "data/census-income-train-clean.txt";
    private static final String TESTING_FILE = "data/census-income-test-clean.txt";
    
    /**
     * Tests out the perceptron with the classic xor test
     * @param args ignored
     */
    public static void main(String[] args) {
        int trainingIterations;
        try
        {
            trainingIterations = Integer.parseInt(args[0]);
        }
        catch (Exception ex)
        {
            System.out.println("Usage:  java -cp ABAGAIL.jar testing.NormalBackPropTest [iterations]");
            return;
        }
        
        Instance[] trainingInstances = initializeTrain();
        Instance[] testingInstances = initializeTest();
        

        // Setup neural network with fixed iterations
        BackPropagationNetworkFactory factory =  new BackPropagationNetworkFactory();
        BackPropagationNetwork network = factory.createClassificationNetwork(new int[] { INPUT_LAYER, HIDDEN_LAYERS, OUTPUT_LAYER });
        DataSet set = new DataSet(trainingInstances);
        FixedIterationTrainer trainer = new FixedIterationTrainer(new BatchBackPropagationTrainer(set, network, new SumOfSquaresError(), new RPROPUpdateRule()), trainingIterations);
        
        // Training
        double start = System.nanoTime();
        trainer.train();
        double end = System.nanoTime();
        double trainingTime = (end - start) / Math.pow(10, 9);

        // Testing
        long correct = 0L, incorrect = 0L;

        start = System.nanoTime();
        for(int i = 0; i < testingInstances.length; i++) {
            network.setInputValues(testingInstances[i].getData());
            network.run();
            
            double nn_out = Double.parseDouble(network.getOutputValues().toString());
            double expected = Double.parseDouble(testingInstances[i].getLabel().toString());
            
            System.out.println("NN_out: "+nn_out+"\n"+"Expected: "+expected+'\n');
            double trash = Math.abs(nn_out - expected) < 0.5 ? correct++ : incorrect++;
        }
        end = System.nanoTime();
        double testingTime = (end - start) / Math.pow(10, 9);


        String results =  "\nResults for normal backpropagation: \nCorrectly classified " + correct + " instances." +
                        "\nIncorrectly classified " + incorrect + " instances.\nPercent correctly classified: "
                        + df.format(((double) correct) / ((double) (correct+incorrect))*100) + "%\nTraining time: " + df.format(trainingTime)
                        + " seconds\nTesting time: " + df.format(testingTime) + " seconds\n";

        
        System.out.println(results);
    }

    private static Instance[] initializeTrain() {

        double[][][] attributes = new double[TRAINING_SET_SIZE][][];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(TRAINING_FILE)));

            for(int i = 0; i < attributes.length; i++) {
                Scanner scan = new Scanner(br.readLine());
                scan.useDelimiter(",");

                attributes[i] = new double[2][];
                attributes[i][0] = new double[9]; //9 attributes
                attributes[i][1] = new double[1];

                for(int j = 0; j < 9; j++)
                    attributes[i][0][j] = Double.parseDouble(scan.next());

                attributes[i][1][0] = Double.parseDouble(scan.next());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        Instance[] instances = new Instance[attributes.length];
        for(int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(attributes[i][0]);
            // either 0 or 1
            instances[i].setLabel(new Instance(attributes[i][1][0]));
        }

        return instances;
    }

    private static Instance[] initializeTest() {

        double[][][] attributes = new double[TESTING_SET_SIZE][][];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(TESTING_FILE)));

            for(int i = 0; i < attributes.length; i++) {
                Scanner scan = new Scanner(br.readLine());
                scan.useDelimiter(",");

                attributes[i] = new double[2][];
                attributes[i][0] = new double[9]; //9 attributes
                attributes[i][1] = new double[1];

                for(int j = 0; j < 9; j++)
                    attributes[i][0][j] = Double.parseDouble(scan.next());

                attributes[i][1][0] = Double.parseDouble(scan.next());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        Instance[] instances = new Instance[attributes.length];
        for(int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(attributes[i][0]);
            instances[i].setLabel(new Instance(attributes[i][1][0]));
        }

        return instances;
    }
}
