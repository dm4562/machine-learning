package opt.test;

import java.util.Arrays;

import dist.DiscreteDependencyTree;
import dist.DiscreteUniformDistribution;
import dist.Distribution;

import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.SingleCrossOver;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 * A test using the flip flop evaluation function
 * @author Andrew Guillory gtg008g@mail.gatech.edu
 * @version 1.0
 */
public class FlipFlopTest {
    /** The n value */
    // private static final int N = 100;

    public static void main(String[] args) {
        int N = 0;
        try {
            N = Integer.parseInt(args[0]);
        } catch (Exception e) {
            System.out.println("Usage: java -cp ABAGAIL.jar opt.test.FlipFlopTest [N value]");
        }
        int[] ranges = new int[N];
        Arrays.fill(ranges, 2);
        EvaluationFunction ef = new FlipFlopEvaluationFunction();
        Distribution odd = new DiscreteUniformDistribution(ranges);
        NeighborFunction nf = new DiscreteChangeOneNeighbor(ranges);
        MutationFunction mf = new DiscreteChangeOneMutation(ranges);
        CrossoverFunction cf = new SingleCrossOver();
        Distribution df = new DiscreteDependencyTree(.1, ranges);
        HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
        GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
        ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);

        testSimulatedAnnealing(hcp, ef);

        System.out.println("N: " + N);
        RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
        FixedIterationTrainer fit = new FixedIterationTrainer(rhc, 200000);
        long starttime = System.currentTimeMillis();
        fit.train();
        System.out.println("RHC: " + ef.value(rhc.getOptimal()));
        System.out.println("RHC Time: " + (System.currentTimeMillis() - starttime));

        SimulatedAnnealing sa = new SimulatedAnnealing(100, 0.95, hcp);
        fit = new FixedIterationTrainer(sa, 200000);
        starttime = System.currentTimeMillis();
        fit.train();
        System.out.println("SA: " + ef.value(sa.getOptimal()));
        System.out.println("SA Time: " + (System.currentTimeMillis() - starttime));

        StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 100, 20, gap);
        fit = new FixedIterationTrainer(ga, 1000);
        starttime = System.currentTimeMillis();
        fit.train();
        System.out.println("GA: " + ef.value(ga.getOptimal()));
        System.out.println("GA Time: " + (System.currentTimeMillis() - starttime));

        MIMIC mimic = new MIMIC(200, 5, pop);
        fit = new FixedIterationTrainer(mimic, 1000);
        starttime = System.currentTimeMillis();
        fit.train();
        System.out.println("MIMIC: " + ef.value(mimic.getOptimal()));
        System.out.println("MIMIC Time: " + (System.currentTimeMillis() - starttime));
        System.out.println();
    }

    public static void testSimulatedAnnealing(HillClimbingProblem hcp, EvaluationFunction ef) {
        double cooling = 0.99;
        while (cooling > 0) {
            SimulatedAnnealing sa = new SimulatedAnnealing(100, cooling, hcp);
            FixedIterationTrainer fit = new FixedIterationTrainer(sa, 20000);
            fit.train();
            System.out.println("COOLING: " + cooling);
            System.out.println("VALUE: " + ef.value(sa.getOptimal()));
            System.out.println();
            cooling -= 0.05;
        }
    }
}
