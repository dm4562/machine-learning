package opt.test;

import opt.*;
import opt.example.*;
import opt.ga.*;
import shared.*;
import func.nn.backprop.*;

import java.util.*;
import java.io.*;
import java.text.*;

/**
 * Implementation of randomized hill climbing, simulated annealing, and genetic algorithm to
 * find optimal weights to a neural network that is classifying eye movements using a training set and a
 * testing set 
 *
 * @author Dhruv Mehra
 * @version 1.0
 */
public class RandomizedOptimizationTests {
    private static Instance[] trainingInstances;
    private static Instance[] testingInstances;

    private static final int TRAINING_SET_SIZE = 30162;
    private static final int TESTING_SET_SIZE = 15060;
    private static final int ATTRIBUTE_COUNT = 14;

    private static final int INPUT_LAYER = ATTRIBUTE_COUNT;
    private static final int HIDDEN_LAYERS = 6;
    private static final int OUTPUT_LAYER = 1;

    private static final int ALGO_COUNT = 3;

    private static String[] oaNames = { "RHC", "SA", "GA" };

    private static final DecimalFormat df = new DecimalFormat("0.000");

    private static final String TRAINING_FILE = "../data_sets/census/census-income-train-clean.txt";
    private static final String TESTING_FILE = "../data_sets/census/census-income-test-clean.txt";

    public static void main(String[] args) {
        int trainingIterations;
        try {
            trainingIterations = Integer.parseInt(args[0]);
        } catch (Exception ex) {
            System.out.println("Usage:  java -cp ABAGAIL.jar testing.RandomizedOptimizationTests [iterations]");
            return;
        }

        // Initialize data sets
        trainingInstances = initializeTraining();
        testingInstances = initializeTesting();
        DataSet set = new DataSet(trainingInstances);

        // Declare training stuff
        BackPropagationNetworkFactory factory = new BackPropagationNetworkFactory();
        ErrorMeasure measure = new SumOfSquaresError();
        BackPropagationNetwork networks[] = new BackPropagationNetwork[ALGO_COUNT];
        NeuralNetworkOptimizationProblem[] nnop = new NeuralNetworkOptimizationProblem[ALGO_COUNT];
        OptimizationAlgorithm[] oa = new OptimizationAlgorithm[ALGO_COUNT];

        for (int i = 0; i < oa.length; i++) {
            networks[i] = factory.createClassificationNetwork(new int[] { INPUT_LAYER, HIDDEN_LAYERS, OUTPUT_LAYER });
            nnop[i] = new NeuralNetworkOptimizationProblem(set, networks[i], measure);
        }

        oa[0] = new RandomizedHillClimbing(nnop[0]);
        // (Initial Temperature, Cooling Rate)
        oa[1] = new SimulatedAnnealing(1E11, .95, nnop[1]);
        // (Population Size, # of population mating every epoch, # of mutations every epoch)
        oa[2] = new StandardGeneticAlgorithm(200, 100, 50, nnop[2]);

        // Initialize output
        StringBuilder results = new StringBuilder();

        for (int i = 0; i < oa.length; i++) {
            // Training
            double start = System.nanoTime();
            train(oa[i], networks[i], measure, oaNames[i], trainingIterations);
            double end = System.nanoTime();
            double trainingTime = (end - start) / 1000000000;
            //trainingTime /= Math.pow(10, 9);

            Instance optimalInstance = oa[i].getOptimal();
            networks[i].setWeights(optimalInstance.getData());

            // Append header for results
            results.append("\n\nResults for " + oaNames[i] + ":\n");

            // Test on training set
            long train_correct = 0L, train_incorrect = 0L;
            for (int j = 0; j < trainingInstances.length; j++) {
                networks[i].setInputValues(trainingInstances[j].getData());
                networks[i].run();

                double predicted = Double.parseDouble(trainingInstances[j].getLabel().toString());
                double actual = Double.parseDouble(networks[i].getOutputValues().toString());
                //System.out.println("Predicted: "+predicted+" | "+"Actual: "+actual);
                double trash = Math.abs(predicted - actual) < 0.5 ? train_correct++ : train_incorrect++;
            }

            results.append("Correctly classified (training set): " + train_correct + " instances.\n");
            results.append("Incorrectly classified (training set): " + train_incorrect + " instances.\n");
            results.append("Percent training set correctly classified: "
                    + df.format(((double) train_correct) / ((double) (train_correct + train_incorrect)) * 100) + "%\n");

            // Testing
            long correct = 0L, incorrect = 0L;

            start = System.nanoTime();
            for (int j = 0; j < testingInstances.length; j++) {
                networks[i].setInputValues(testingInstances[j].getData());
                networks[i].run();

                double predicted = Double.parseDouble(testingInstances[j].getLabel().toString());
                double actual = Double.parseDouble(networks[i].getOutputValues().toString());
                //System.out.println("Predicted: "+predicted+" | "+"Actual: "+actual);
                double trash = Math.abs(predicted - actual) < 0.5 ? correct++ : incorrect++;
            }
            end = System.nanoTime();
            double testingTime = (end - start) / 1000000000;
            // testingTime /= Math.pow(10, 9);

            results.append("\nCorrectly classified " + correct + " instances." + "\nIncorrectly classified " + incorrect
                    + " instances.\nPercent correctly classified: "
                    + df.format(((double) correct) / ((double) (correct + incorrect)) * 100) + "%\nTraining time: "
                    + df.format(trainingTime) + " seconds\nTesting time: " + df.format(testingTime) + " seconds\n");
        }

        System.out.println(results.toString());
    }

    private static void train(OptimizationAlgorithm oa, BackPropagationNetwork network, ErrorMeasure measure,
            String oaName, int trainingIterations) {
        System.out.println("\nError results for " + oaName + "\n---------------------------");

        for (int i = 0; i < trainingIterations; i++) {
            oa.train();

            double error = 0;
            for (int j = 0; j < trainingInstances.length; j++) {
                network.setInputValues(trainingInstances[j].getData());
                network.run();

                Instance output = trainingInstances[j].getLabel(), example = new Instance(network.getOutputValues());
                example.setLabel(new Instance(Double.parseDouble(network.getOutputValues().toString())));
                error += measure.value(output, example);
            }

            System.out.println(df.format(error));
        }
    }

    private static Instance[] initializeTraining() {

        double[][][] attributes = new double[TRAINING_SET_SIZE][][];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(TRAINING_FILE)));

            for (int i = 0; i < attributes.length; i++) {
                Scanner scan = new Scanner(br.readLine());
                scan.useDelimiter(",");

                attributes[i] = new double[2][];
                attributes[i][0] = new double[ATTRIBUTE_COUNT]; // attribute count
                attributes[i][1] = new double[1];

                for (int j = 0; j < ATTRIBUTE_COUNT; j++)
                    attributes[i][0][j] = Double.parseDouble(scan.next());

                attributes[i][1][0] = Double.parseDouble(scan.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instance[] instances = new Instance[attributes.length];
        for (int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(attributes[i][0]);
            // classifications range from 0 to 30; split into 0 - 14 and 15 - 30
            instances[i].setLabel(new Instance(attributes[i][1][0]));
        }

        return instances;
    }

    private static Instance[] initializeTesting() {

        double[][][] attributes = new double[TESTING_SET_SIZE][][];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(TESTING_FILE)));

            for (int i = 0; i < attributes.length; i++) {
                Scanner scan = new Scanner(br.readLine());
                scan.useDelimiter(",");

                attributes[i] = new double[2][];
                attributes[i][0] = new double[ATTRIBUTE_COUNT]; // attribute count
                attributes[i][1] = new double[1];

                for (int j = 0; j < ATTRIBUTE_COUNT; j++)
                    attributes[i][0][j] = Double.parseDouble(scan.next());

                attributes[i][1][0] = Double.parseDouble(scan.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Instance[] instances = new Instance[attributes.length];
        for (int i = 0; i < instances.length; i++) {
            instances[i] = new Instance(attributes[i][0]);
            instances[i].setLabel(new Instance(attributes[i][1][0]));
        }

        return instances;
    }
}