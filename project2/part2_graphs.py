import matplotlib.pyplot as plt
import numpy as np

plt.style.use('ggplot')


def plot(xvals, rhc, sa, ga, mimic, title, xlabel, ylabel):
    fig = plt.figure()
    plt.plot(xvals, rhc, label="Randomized Hill Climbing")
    plt.plot(xvals, sa, label="Simulated Annealing")
    plt.plot(xvals, ga, label="Genetic Algorithm")
    plt.plot(xvals, mimic, label="MIMIC Algorithm")
    plt.suptitle(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(loc='best')
    fig.savefig('graphs/{}.png'.format(title.replace(" ", "_").lower()))


def parse_file(filename):
    rhc, sa, ga, mimic = [], [], [], []
    rhc_time, sa_time, ga_time, mimic_time = [], [], [], []

    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split(':')
            if not parts[0] or parts[0] == 'N':
                continue

            if parts[0] == 'RHC':
                rhc.append(float(parts[1].strip()))
            elif parts[0] == 'RHC Time':
                rhc_time.append(float(parts[1].strip()))
            elif parts[0] == 'SA':
                sa.append(float(parts[1].strip()))
            elif parts[0] == 'SA Time':
                sa_time.append(float(parts[1].strip()))
            elif parts[0] == 'GA':
                ga.append(float(parts[1].strip()))
            elif parts[0] == 'GA Time':
                ga_time.append(float(parts[1].strip()))
            elif parts[0] == 'MIMIC':
                mimic.append(float(parts[1].strip()))
            elif parts[0] == 'MIMIC Time':
                mimic_time.append(float(parts[1].strip()))

    return ((rhc, sa, ga, mimic), (rhc_time, sa_time, ga_time, mimic_time))


def travelingsalesman():
    filename = 'output/part2/travelingsalesman.txt'

    xvals = [x for x in range(5, 51, 5)]
    temp = parse_file(filename)

    rhc, sa, ga, mimic = temp[0]
    rhc_time, sa_time, ga_time, mimic_time = temp[1]

    assert(len(xvals) == len(rhc))
    plot(xvals, rhc, sa, ga, mimic, "Number of nodes vs Inverse of distance",
         "Number of nodes", "Inverse of distance")

    plot(xvals, rhc_time, sa_time, ga_time, mimic_time, "Number of nodes vs Time taken",
         "Number of nodes", "Time taken")


def flipflop():
    filename = 'output/part2/flipflop.txt'

    xvals = [x for x in range(50, 501, 50)]

    temp = parse_file(filename)
    rhc, sa, ga, mimic = temp[0]
    rhc_time, sa_time, ga_time, mimic_time = temp[1]

    assert(len(xvals) == len(rhc))
    plot(xvals, rhc, sa, ga, mimic, "Length of string vs Fitness score",
         "Length of string", "Fitness score")

    plot(xvals, rhc_time, sa_time, ga_time, mimic_time, "Length of string vs Time taken",
         "Length of string", "Time taken")


def simulated_annealing():
    filename = 'output/part2/simulatedannealing.txt'

    xvals = [x for x in np.arange(0.99, 0, -0.05)]
    yvals = []

    with open(filename) as file:
        for line in file:
            parts = line.strip().split(':')

            if not parts[0] or parts[0] == 'COOLING':
                continue

            yvals.append(float(parts[1].strip()))

    assert(len(yvals) == len(xvals))
    fig = plt.figure()
    plt.plot(xvals, yvals)
    plt.suptitle("Cooling vs Fitness function")
    plt.xlabel("Cooling value")
    plt.ylabel("Fitness score")
    fig.savefig("graphs/cooling_vs_fitness.png")


def fourpeaks():
    filename = 'output/part2/fourpeaks.txt'

    xvals = [50, 100, 200, 300, 400, 600, 800, 1000, 1300, 1600]
    temp = parse_file(filename)

    rhc, sa, ga, mimic = temp[0]
    rhc_time, sa_time, ga_time, mimic_time = temp[1]

    assert(len(xvals) == len(sa))

    plot(xvals, rhc, sa, ga, mimic, "Number of iterations vs Fitness function",
         "Number of Iterations", "Fitness score")


def mimic():
    filename = 'output/part2/mimic.txt'

    xvals = [x for x in range(50, 401, 10)]
    yvals = []

    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split(":")

            if not parts[0] or parts[0] == 'SAMPLES':
                continue

            yvals.append(float(parts[1].strip()))

    assert(len(yvals) == len(xvals))

    fig = plt.figure()
    plt.plot(xvals, yvals)
    plt.suptitle("Sample size vs Fitness function")
    plt.xlabel("Sample size")
    plt.ylabel("Fitness function")
    fig.savefig("graphs/sample_size_vs_fitness.png")


def main():
    travelingsalesman()
    flipflop()
    simulated_annealing()
    fourpeaks()
    mimic()

if __name__ == '__main__':
    main()
