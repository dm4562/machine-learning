import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder

plt.style.use('ggplot')

def kvalues_graph(train, test, name):
    print("=" * 20 + " K VALUES GRAPH FOR {} ".format(name.upper()) + "=" * 20)
    train_x, train_y = train
    test_x, test_y = test

    k_values = [x for x in range(1, 100, 2)]
    train_error, test_error, valid_error = [], [], []

    for k in k_values:
        clf = KNeighborsClassifier(n_neighbors=k)
        scores = cross_val_score(clf, train_x, train_y, cv=10)
        clf.fit(train_x, train_y)

        y = clf.predict(train_x)
        y_ = clf.predict(test_x)

        v_e = 1 - scores.mean()
        tr_e = 1 - accuracy_score(train_y, y)
        te_e = 1 - accuracy_score(test_y, y_)

        train_error.append(100 * tr_e)
        test_error.append(100 * te_e)
        valid_error.append(100 * v_e)

        print("K: {}".format(k))
        print("Testing error: {}".format(te_e))
        print("Training error: {}".format(tr_e))
        print("Validation error: {}\n".format(v_e))

    fig = plt.figure()
    plt.plot(k_values, train_error, label='Training error')
    plt.plot(k_values, test_error, label='Testing error')
    plt.plot(k_values, valid_error, label='Validation error')
    plt.xlabel('K values')
    plt.ylabel('Percentage error')
    plt.suptitle('{} Model Complexity'.format(name.title()))
    plt.legend(loc='best')
    fig.savefig('knn/{}_k_values.png'.format(name.replace(' ', '_').lower()))
    #plt.show()

def learning_curve(train, test, name, attr):
    print("=" * 20 + " LEARNING CURVE OF {} ".format(name.upper()) + "=" * 20)

    test_x, test_y = test
    steps = [x / 10 for x in range(1, 10)]

    test_error, train_error, valid_error = [], [], []
    for step in steps:
        temp_train, _ = train_test_split(train, test_size=1 - step)
        Y = temp_train[attr]
        X = temp_train[[x for x in temp_train.columns if x != attr]]

        clf = KNeighborsClassifier(n_neighbors=2)
        scores = cross_val_score(clf, X, Y, cv=10)
        clf.fit(X, Y)

        v_e = 1 - scores.mean()
        tr_e = 1 - accuracy_score(Y, clf.predict(X))
        te_e = 1 - accuracy_score(test_y, clf.predict(test_x))

        test_error.append(100 * te_e)
        train_error.append(100 * tr_e)
        valid_error.append(100 * v_e)
    
    Y = train[attr]
    X = train[[x for x in train.columns if x != attr]]

    clf = KNeighborsClassifier(n_neighbors=2)
    scores = cross_val_score(clf, X, Y, cv=10)
    clf.fit(X, Y)

    v_e = 1 - scores.mean()
    tr_e = 1 - accuracy_score(Y, clf.predict(X))
    te_e = 1 - accuracy_score(test_y, clf.predict(test_x))

    test_error.append(100 * te_e)
    train_error.append(100 * tr_e)
    valid_error.append(100 * v_e)
    steps.append(1.0)

    fig = plt.figure()
    plt.plot(steps, train_error, label='Training error')
    plt.plot(steps, test_error, label='Testing error')
    # plt.plot(steps, valid_error, label='Validation error')
    plt.xlabel('Percentage Training set used')
    plt.ylabel('Percentage error')
    plt.suptitle('{} Learning Curve'.format(name.title()), fontsize=18)
    plt.legend(loc='best')
    fig.savefig('knn/{}_learning_curve.png'.format(name.replace(' ', '_').lower()))

def main():
    HR_DATA_FILE = '../data_sets/human_resource/HR_data.csv'
    hr_data = pd.read_csv(HR_DATA_FILE)
    le = LabelEncoder()
    hr_data['department'] = le.fit_transform(hr_data['department'])
    hr_data['salary'] = le.fit_transform(hr_data['salary'])

    hr_train, hr_test = train_test_split(hr_data, test_size=0.20)

    hr_train_y = hr_train['left_binarized']
    hr_train_x = hr_train[[x for x in hr_train.columns if x != 'left_binarized']]

    hr_test_y = hr_test['left_binarized']
    hr_test_x = hr_test[[x for x in hr_test.columns if x != 'left_binarized']]

    hr_name = 'human resource'
    hr_attr= 'left_binarized'

    ###############################################################################

    CHARACTER_DATA_FILE = '../data_sets/letter_recognition/letter_recognition.csv'
    char_data = pd.read_csv(CHARACTER_DATA_FILE)

    char_train, char_test = train_test_split(char_data, test_size=0.25)

    char_train_y = char_train['lettr']
    char_train_x = char_train[[x for x in char_train.columns if x != 'lettr']]

    char_test_y = char_test['lettr']
    char_test_x = char_test[[x for x in char_train.columns if x != 'lettr']]

    char_name = 'character recognition'
    char_attr = 'lettr'

    kvalues_graph((hr_train_x, hr_train_y), (hr_test_x, hr_test_y), hr_name)
    kvalues_graph((char_train_x, char_train_y), (char_test_x, char_test_y), char_name)
    #learning_curve(hr_data, (hr_test_x, hr_test_y), hr_name, hr_attr)
    #learning_curve(char_data, (char_test_x, char_test_y), char_name, char_attr)

if __name__ == '__main__':
    main()
