===Decision Trees===
==Human Resource Dataset==
weka.classifiers.trees.J48 -C 0.25 -M 2

==Character Recognition Dataset==
weka.classifiers.trees.J48 -C 0.25 -M 2

For rest of the algorithms python scripts were written to plot the 
metrics.

Interpreter: Python 3.6
Libraries:
- Numpy
- Scipy
- Scikit-learn
- Matplotlib
- Pandas

==Boosting==
python boosting.py
Results Folder: boosting

==Conclusion==
python conclusion.py
Results Folder: conclusion

==K Nearest Neighbor==
python knn.py
Results Folder: knn

==Neural Networdk==
python multilayer_perceptron.py
Results Folder: neural_net

==Support Vector Machines==
python svm.py
Results Folder: svm