import matplotlib.pyplot as plt
from numpy import arange
plt.style.use('ggplot')

character = [86.3, 78.4, 95.4, 97.1, 55.7]
human_resource = [97.5, 94.5, 94.7, 94.3, 96.4]

kernels = ['Decision Tree','Neural Net', 'KNN', 'SVM', 'Boosting']
temp_x = arange(5)
fig = plt.figure()
plt.style.use('ggplot')
plt.bar(temp_x - 0.22, human_resource, width=0.20, label="Human Resource")
plt.bar(temp_x + 0.02, character, width=0.20, label="Character Recognition")
plt.xticks(temp_x, kernels)
plt.xlabel('Algorithms')
plt.ylabel('Best Accuracy')
plt.legend(loc='best')
plt.title('Supervised Learning Algorithms Compared')
fig.savefig('conclusion/conclusion.png')
plt.close(fig)