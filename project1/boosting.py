import pandas as pd
import matplotlib.pyplot as plt 
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import cross_val_score

plt.style.use('ggplot')

def estimator_graph(test, train, name):
    train_x, train_y = train
    test_x, test_y = test

    training_accuracy = []
    validation_accuracy = []
    test_accuracy = []
    n_estimators = range(1, 51)

    # For N estimators in Boosting
    for n in n_estimators:
        clf = AdaBoostClassifier(n_estimators=n, random_state=1)
            
        cv = cross_val_score(clf, train_x, train_y, cv=10).mean()
        clf.fit(train_x, train_y)

        validation_accuracy.append(100 * (1 - cv))
        training_accuracy.append(100 * (1 - accuracy_score(train_y, clf.predict(train_x))))
        test_accuracy.append(100 * (1 - accuracy_score(test_y, clf.predict(test_x))))

    fig = plt.figure()
    plt.plot(n_estimators, training_accuracy, label="Training error")
    plt.plot(n_estimators, validation_accuracy, label="Cross Validation error")
    plt.plot(n_estimators, test_accuracy, label="Testing error")
    plt.xlabel('Number of Estimators')
    plt.ylabel('Percentage error')
    plt.legend(loc='best')
    plt.title('{} Estimators'.format(name.title()))
    fig.savefig('boosting/{}_bosting_estimator.png'.format(name.replace(' ', '_').lower()))
    plt.close(fig)

def learning_curve(train, test, name, attr, estimators):
    test_x, test_y = test
    
    # After finding the right estimator number, experiment on training set size
    training_accuracy = []
    validation_accuracy = []
    test_accuracy = []
    training_size = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    for s in training_size:
        # Define the classifier
        clf = AdaBoostClassifier(n_estimators=100, random_state=1)
        
        temp_train, _ = train_test_split(train, test_size= 1 - s, random_state=1)

        # Train set
        Y = temp_train[attr]
        X = temp_train[[x for x in train.columns if attr not in x]]

        clf.fit(X, Y)

        training_accuracy.append(100 * (1 - accuracy_score(Y, clf.predict(X))))
        test_accuracy.append(100 * (1 - accuracy_score(test_y, clf.predict(test_x))))
        cv = 1 - cross_val_score(clf, X, Y, cv=10).mean()
        validation_accuracy.append(100 * cv)

    train_y = train[attr]
    train_x = train[[x for x in train.columns if x != attr]]
    
    clf = AdaBoostClassifier(n_estimators=100, random_state=1)
    clf.fit(train_x, train_y)

    training_accuracy.append(100 * (1 - accuracy_score(train_y, clf.predict(train_x))))
    cv = cross_val_score(clf, train_x, train_y, cv=7).mean()
    validation_accuracy.append(100 * (1 - cv))
    test_accuracy.append(100 * (1 - accuracy_score(test_y, clf.predict(test_x))))
    training_size.append(1)

    fig = plt.figure()
    line1, = plt.plot(training_size, training_accuracy, label="Training Accuracy")
    line2, = plt.plot(training_size, validation_accuracy, label="Cross Validation Score")
    line1, = plt.plot(training_size, test_accuracy, label="Testing Accuracy")
    plt.xlabel('Training Set Size (%)')
    plt.ylabel('Percent error')
    plt.title('{} Learning Curve'.format(name.title()))
    plt.legend(loc='best')
    fig.savefig('boosting/{}_learning_curve.png'.format(name.replace(' ', '_').lower()))
    plt.close(fig)

def main():
    HR_DATA_FILE = '../data_sets/human_resource/HR_data.csv'
    CHARACTER_DATA_FILE = '../data_sets/letter_recognition/letter_recognition.csv'

    hr_data = pd.read_csv(HR_DATA_FILE)
    le = LabelEncoder()
    hr_data['department'] = le.fit_transform(hr_data['department'])
    hr_data['salary'] = le.fit_transform(hr_data['salary'])

    hr_train, hr_test = train_test_split(hr_data, test_size=0.20)

    hr_train_y = hr_train['left_binarized']
    hr_train_x = hr_train[[x for x in hr_train.columns if x != 'left_binarized']]

    hr_test_y = hr_test['left_binarized']
    hr_test_x = hr_test[[x for x in hr_test.columns if x != 'left_binarized']]
    hr_name = 'human resource'
    hr_attr= 'left_binarized'

    ###############################################################################

    char_data = pd.read_csv(CHARACTER_DATA_FILE)
    # labelencoder = LabelEncoder()
    # char_data['lettr'] = labelencoder.fit_transform(char_data['lettr'])

    char_train, char_test = train_test_split(char_data, test_size=0.25)

    char_train_y = char_train['lettr']
    char_train_x = char_train[[x for x in char_train.columns if x != 'lettr']]

    char_test_y = char_test['lettr']
    char_test_x = char_test[[x for x in char_train.columns if x != 'lettr']]

    char_name = 'character recognition'
    char_attr = 'lettr'

    # estimator_graph((char_test_x, char_test_y), (char_train_x, char_train_y), char_name)
    # estimator_graph((hr_test_x, hr_test_y), (hr_train_x, hr_train_y), hr_name)
    # learning_curve(char_train, (char_test_x, char_test_y), char_name, char_attr, 13)
    learning_curve(hr_train, (hr_test_x, hr_test_y), hr_name, hr_attr, 35)

if __name__ == '__main__':
    main()
    
