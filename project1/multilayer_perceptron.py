"""
A multilayer perceptron implementation using SkLearn
"""

import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

plt.style.use('ggplot')



def model_complexity_graph(neurons, train_x, train_y, name):
    print("=" * 20 + " MODEL COMPLEXITY GRAPH FOR {} ".format(name.upper()) + "=" * 20)
    validation_error, training_error = [], []
    total_layers = [x for x in range(6)]

    for hidden_layers in total_layers:
        model = MLPClassifier(hidden_layer_sizes=layers,
                              random_state=1, activation="logistic", max_iter=1000)
        model = MLPClassifier(hidden_layer_sizes=layers,
                              random_state=1, activation="logistic", max_iter=1000)

        scores = cross_val_score(model, train_x, train_y, cv=10)
        model.fit(train_x, train_y)
        y_ = model.predict(train_x)

        ve = 1 - scores.mean()
        te = 1 - accuracy_score(train_y, y_)

        validation_error.append(ve * 100)
        training_error.append(te * 100)

        print("Layers: {}".format(hidden_layers))
        print("Validation error: {}".format(ve))
        print("Training error: {}\n".format(te))

    fig = plt.figure()
    plt.plot(total_layers, validation_error)
    plt.plot(total_layers, training_error)
    plt.xlabel('Number of hidden layers')
    plt.ylabel('Percentage error')
    fig.savefig(
        'neural_net/{}_model_complexity.png'.format(name.replace(' ', '_').lower()))

    plt.legend(['Validation Error', 'Training Error'], loc='best')
    fig.savefig(
        'neural_net/{}_model_complexity.png'.format(name.replace(' ', '_').lower()))


def learning_curve(number_layers, neurons, train, test_x, test_y, name, attr):
    print("=" * 20 + " LEARNING CURVE GRAPH FOR {} ".format(name.upper()) + "=" * 20)

    layers = tuple([neurons] * number_layers)
        temp_train, _ = train_test_split(train, test_size=1 - step)
    train_error, test_error = [], []

    for step in steps:
        temp_train, _ = train_test_split(train, test_size=1 - step)
        model = MLPClassifier(layers, activation="logistic",
                              random_state=1, max_iter=1500)
        Y = temp_train[attr]
        X = temp_train[[x for x in temp_train.columns if x != attr]]

        model = MLPClassifier(layers, activation="logistic",
                              random_state=1, max_iter=1500)
        result = cross_val_score(model, X, Y, cv=10)
        model.fit(X, Y)
        y_ = model.predict(test_x)

        tr_e = 1 - result.mean()
        te_e = 1 - accuracy_score(test_y, y_)

        train_error.append(tr_e * 100)
        test_error.append(te_e * 100)
    model = MLPClassifier(layers, activation="logistic",
                          random_state=1, max_iter=1500)
        print('Percentage Training: {}'.format(step))
        print('Training Error: {}'.format(tr_e))
        print('Testing Error: {}\n'.format(te_e))

    model = MLPClassifier(layers, activation="logistic",
                          random_state=1, max_iter=1500)
    model.fit(train_x, train_y)
    y = model.predict(train_x)
    y_ = model.predict(test_x)

    tr_e = 1 - accuracy_score(train_y, y)
    te_e = 1 - accuracy_score(test_y, y_)

    steps.append(1.0)
    train_error.append(tr_e * 100)
    test_error.append(te_e * 100)

    print('Percentage Training: {}'.format(step))
    print('Training Error: {}'.format(tr_e))
    print('Testing Error: {}\n'.format(te_e))

    fig = plt.figure()
    fig.savefig(
        'neural_net/{}_learning_curve.png'.format(name.replace(' ', '_').lower()))

    plt.plot(steps, train_error, label='Training error')
    plt.xlabel('Percentage of Training set used')
    plt.ylabel('Percentage error')
    plt.suptitle('{} Learning Curve'.format(name.title()), fontsize=18)
    plt.legend(loc='best')
    learning_rates = [0.02 / (2 ** x) for x in range(8)]
        'neural_net/{}_learning_curve.png'.format(name.replace(' ', '_').lower()))


def time_rate_accuracy(neurons, num_layers, train_x, train_y, test_x, test_y, name):
        model = MLPClassifier(hidden_layer_sizes=layers, activation="logistic", solver='adam',
                              max_iter=500, learning_rate_init=rate, learning_rate="constant")
    layers = tuple([neurons] * num_layers)
    learning_rates = [0.02 / (2 ** x) for x in range(8)]
    iterations = []
    train_error, validation_error, test_error = [], [], []

    for rate in learning_rates:
        model = MLPClassifier(hidden_layer_sizes=layers, activation="logistic", solver='adam',
                              max_iter=500, learning_rate_init=rate, learning_rate="constant")
        scores = cross_val_score(model, train_x, train_y, cv=10)

        model.fit(train_x, train_y)
        y = model.predict(train_x)
        y_ = model.predict(test_x)

        i = model.n_iter_
        ve = 1 - scores.mean()
        tr_e = 1 - accuracy_score(y, train_y)
        te_e = 1 - accuracy_score(y_, test_y)

        iterations.append(i)
        validation_error.append(ve * 100)
        train_error.append(tr_e * 100)
        test_error.append(te_e * 100)

        print("Learning rate: {}".format(rate))
        print("Iterations: {}".format(i))
        print("Validation error: {}".format(ve))
        print("Training error: {}".format(tr_e))
        print("Testing error: {}\n".format(te_e))

    fig.savefig(
        'neural_net/{}_rate_accuracy.png'.format(name.replace(' ', '_').lower()))
    plt.plot(learning_rates, train_error, label='Training error')
    plt.plot(learning_rates, test_error, label='Testing error')
    plt.plot(learning_rates, validation_error, label='Validation error')
    plt.xlabel('Learning rate')
    plt.ylabel('Percentage error')
    plt.legend(loc='best')
    plt.suptitle('{} Learning Rate Curve'.format(name.title()))
    fig.savefig(
        'neural_net/{}_time_accuracy.png'.format(name.replace(' ', '_').lower()))
        'neural_net/{}_rate_accuracy.png'.format(name.replace(' ', '_').lower()))

    fig = plt.figure()
    plt.plot(learning_rates, iterations, label='Number of iterations')
    plt.xlabel('Learning Rates')
    plt.ylabel('Iterations')
    plt.legend(loc='best')
    plt.suptitle('{} Training Time curve'.format(name.title()), fontsize=18)
    fig.savefig(
        'neural_net/{}_time_accuracy.png'.format(name.replace(' ', '_').lower()))

HR_DATA_FILE = '../data_sets/human_resource/HR_data.csv'
CHARACTER_DATA_FILE = '../data_sets/letter_recognition/letter_recognition.csv'

data = pd.read_csv(HR_DATA_FILE)
le = LabelEncoder()
data['department'] = le.fit_transform(data['department'])
data['salary'] = le.fit_transform(data['salary'])

train, test = train_test_split(data, test_size=0.20)

# Train set
train_y = train['left_binarized']
train_x = train[[x for x in train.columns if x != 'left_binarized']]

# Test set
test_y = test['left_binarized']
test_x = test[[x for x in test.columns if x != 'left_binarized']]

neurons = int((len(train_x.columns) + 2) / 2)
hr_name = 'human resource'
attr = 'left_binarized'

# model_complexity_graph(neurons, train_x, train_y, hr_name)
# learning_curve(2, neurons, train, test_x, test_y, hr_name, attr)
# time_rate_accuracy(neurons, 2, train_x, train_y, test_x, test_y, hr_name)

###############################################################################

data = pd.read_csv(CHARACTER_DATA_FILE)

train, test = train_test_split(data, test_size=0.25)

train_y = train['lettr']
train_x = train[[x for x in train.columns if x != 'lettr']]

test_y = test['lettr']
test_x = test[[x for x in train.columns if x != 'lettr']]

neurons = int((len(train_x.columns) + 2) / 2)
char_name = 'character recognition'
attr = 'lettr'
# model_complexity_graph(neurons, train_x, train_y, char_name)
# learning_curve(2, neurons, train, test_x, test_y, char_name, attr)
learning_curve(0, neurons, train, test_x, test_y, char_name, attr)
