import pandas as pd
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import BaggingClassifier
import matplotlib.pyplot as plt

plt.style.use('ggplot')

def model_complexity(train, test, name, attr):
    print("=" * 20 + " MODEL COMPLEXITY GRAPH FOR {} ".format(name.upper()) + "=" * 20)

    train, valid = train_test_split(train, test_size=0.2)

    train_x = train[[x for x in train.columns if x != attr]]
    train_y = train[attr]

    valid_x = valid[[x for x in valid.columns if x != attr]]
    valid_y = valid[attr]

    test_x, test_y = test
    kernels = ['linear', 'poly', 'rbf', 'sigmoid']
    train_error, validation_error, test_error = [], [], []

    for k in kernels:
        n_estimators = 10
        clf = SVC(kernel=k, random_state=1)
        # clf = BaggingClassifier(model, n_estimators=n_estimators, max_samples=1.0/n_estimators, random_state=1)
        # scores = cross_val_score(clf, train_x, train_y, cv=10)

        clf.fit(train_x, train_y)
        y = clf.predict(train_x)
        y_ = clf.predict(test_x)
        Y = clf.predict(valid_x)

        ve = 1 - accuracy_score(valid_y, Y)
        # ve = 1 - scores.mean()
        tr_e = 1 - accuracy_score(train_y, y)
        te_e = 1 - accuracy_score(test_y, y_)

        train_error.append(tr_e)
        validation_error.append(ve)
        test_error.append(te_e)

        print("Kernel: {}".format(k))
        print("Validation error: {}".format(ve))
        print("Testing error: {}".format(te_e))
        print("Training error: {}\n".format(tr_e))

    temp = [_ for _ in range(len(kernels))]
    fig = plt.figure()
    plt.plot(temp, train_error, label='Training error')
    # plt.plot(temp, test_error, label='Testing error')
    plt.plot(temp, validation_error, label='Validation error')
    plt.xticks(temp, kernels)
    plt.xlabel('Kernel used')
    plt.ylabel('Percentage error')
    plt.suptitle('{} Model Complexity'.format(name.title()), fontsize=18)
    plt.legend(loc='best')
    fig.savefig('svm/{}_model_complexity_curve.png'.format(name.replace(' ', '_').lower()))
    # plt.show()

def learning_curve(train, test, kernel, name, attr):
    print("=" * 20 + " LEARNING CURVE FOR {} ".format(name.upper()) + "=" * 20)

    steps = [x / 10 for x in range(1, 10)]
    test_x, test_y = test
    train_error, test_error = [], []

    train, valid = train_test_split(train, test_size=0.2)
    valid_x = valid[[x for x in valid.columns if x != attr]]
    valid_y = valid[attr]

    train_error, validation_error, test_error = [], [], []

    for step in steps:
        temp_train, _ = train_test_split(train, test_size=1 - step) 

        Y = temp_train[attr]
        X = temp_train[[x for x in temp_train.columns if x != attr]]

        clf = SVC(kernel=kernel, random_state=1)
        # result = cross_val_score(clf, X, Y, cv=10)
        clf.fit(X, Y)

        y = clf.predict(X)
        y_ = clf.predict(test_x)
        vy = clf.predict(valid_x)

        v_e = 1 - accuracy_score(valid_y, vy)
        tr_e = 1 - accuracy_score(Y, y)
        te_e = 1 - accuracy_score(test_y, y_)

        train_error.append(100 * tr_e)
        validation_error.append(100 * v_e)
        test_error.append(100 * te_e)

        print("Percentage Training set: {}".format(step))
        print("Validation error: {}".format(v_e))
        print("Testing error: {}".format(te_e))
        print("Training error: {}\n".format(tr_e))
    
    Y = train[attr]
    X = train[[x for x in temp_train.columns if x != attr]]
    clf = SVC(kernel=kernel, random_state=1)
    # result = cross_val_score(clf, X, Y, cv=10)
    clf.fit(X, Y)

    y = clf.predict(X)
    y_ = clf.predict(test_x)
    vy = clf.predict(valid_x)

    v_e = 1 - accuracy_score(valid_y, vy)
    tr_e = 1 - accuracy_score(Y, y)
    te_e = 1 - accuracy_score(test_y, y_)

    train_error.append(100 * tr_e)
    validation_error.append(100 * v_e)
    test_error.append(100 * te_e)
    steps.append(1.0)

    print("Percentage Training set: {}".format(1.0))
    print("Validation error: {}".format(v_e))
    print("Testing error: {}".format(te_e))
    print("Training error: {}\n".format(tr_e))

    fig = plt.figure()
    plt.plot(steps, train_error, label='Training error')
    plt.plot(steps, test_error, label='Testing error')
    #plt.plot(steps, validation_error, label='Validation error')
    plt.xlabel('Percentage Training set used')
    plt.ylabel('Percentage error')
    plt.suptitle('{} Learning Curve'.format(name.title()), fontsize=18)
    plt.legend(loc='best')
    fig.savefig('svm/{}_learning_curve.png'.format(name.replace(' ', '_').lower()))
    
def penalty_accuracy_curve(train, test, name):
    print("=" * 20 + " PENALTY ACCURACY GRAPH FOR {} ".format(name.upper()) + "=" * 20)

    c_vals = [2**(x) for x in range(-4, 5)]
    train_x, train_y = train
    test_x, test_y = test

    train_error, test_error, valid_error = [], [], []
    for c in c_vals:
        clf = SVC(C=c, kernel='rbf', random_state=1)
        results = cross_val_score(clf, train_x, train_y, cv=10)

        clf.fit(train_x, train_y)
        y = clf.predict(train_x)
        y_ = clf.predict(test_x)

        v_e = 1 - results.mean()
        tr_e = 1 - accuracy_score(y, train_y)
        te_e = 1 - accuracy_score(y_, test_y)

        train_error.append(100 * tr_e)
        test_error.append(100 * te_e)
        valid_error.append(100 * v_e)

    fig = plt.figure()
    plt.plot(c_vals, train_error, label='Training Error')
    plt.plot(c_vals, test_error, label='Testing error')
    plt.plot(c_vals, valid_error, label='Validation error')
    plt.suptitle('{} Penalty Level Graph'.format(name.title()))
    plt.xlabel('C penalty values')
    plt.ylabel('Percentage error')
    plt.legend(loc='best')
    fig.savefig('svm/{}_penalty_curve.png'.format(name.replace(' ', '_').lower()))


HR_DATA_FILE = '../data_sets/human_resource/HR_data.csv'
CHARACTER_DATA_FILE = '../data_sets/letter_recognition/letter_recognition.csv'

hr_data = pd.read_csv(HR_DATA_FILE)
le = LabelEncoder()
hr_data['department'] = le.fit_transform(hr_data['department'])
hr_data['salary'] = le.fit_transform(hr_data['salary'])

hr_train, hr_test = train_test_split(hr_data, test_size=0.20)

hr_train_y = hr_train['left_binarized']
hr_train_x = hr_train[[x for x in hr_train.columns if x != 'left_binarized']]

hr_test_y = hr_test['left_binarized']
hr_test_x = hr_test[[x for x in hr_test.columns if x != 'left_binarized']]
hr_name = 'human resource'
hr_attr= 'left_binarized'

###############################################################################

char_data = pd.read_csv(CHARACTER_DATA_FILE)

char_train, char_test = train_test_split(char_data, test_size=0.25)

char_train_y = char_train['lettr']
char_train_x = char_train[[x for x in char_train.columns if x != 'lettr']]

char_test_y = char_test['lettr']
char_test_x = char_test[[x for x in char_train.columns if x != 'lettr']]

char_name = 'character recognition'
char_attr = 'lettr'

model_complexity(hr_train, (hr_test_x, hr_test_y), hr_name, hr_attr)
model_complexity(char_train, (char_test_x, char_test_y), char_name, char_attr)
learning_curve(char_train, (char_test_x, char_test_y), 'rbf', char_name, char_attr)
learning_curve(hr_train, (hr_test_x, hr_test_y), 'rbf', hr_name, hr_attr)

penalty_accuracy_curve((char_train_x, char_train_y), (char_test_x, char_test_y), char_name)
penalty_accuracy_curve((hr_train_x, hr_train_y), (hr_test_x, hr_test_y), hr_name)
# penalty_accuracy_curve((char_train_x, char_train_y), (char_test_x, char_test_y), char_name)
