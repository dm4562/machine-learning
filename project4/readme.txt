Carnegie Mellon University's RLSim library was used in the assignment.
Execute the following command to run the RLSim library.

$ java -jar RL_Sim/rl_sim.jar

All the results of the experiments can be found in the results directory.