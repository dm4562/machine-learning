# CS 4642 - Intro to Machine Learning

This repo contains all the projects completed by me in Spring 17 during the course CS 4641 taught by Charles Isbell.

## Project 1 - Supervised Learning

This project focused using 5 algorithms to solve a machine learning problem and learning the principles of Supervised Learning. The algorithms used in this peoject were:

1. Decision Trees
2. Neural Networks
3. Boosting
4. Support Vector Machines
5. k - Nearest Neighbor

For more information about the ML problem chosen, results and analysis of the same check the [report](https://gitlab.com/dm4562/machine-learning/blob/master/project1/dmehra6-analysis.pdf) in `project1` directory.

## Project 2 - Randomized Optimization

The purpose of this project was to explore random search. The local random search algorithms used in this project were:

1. Randomized Hill Climbing
2. Simulated Annealing
3. A Genetic Algorithm
4. MIMIC

The first 3 algorithms were used to find good weights for a neural network instead of backprop. Then these 4 optimization algorithms were used to solve 3 optimization problems:

- Traveling Salesman
- Flip Flop Problem
- Four Peaks

To learn more about the methodology used, results obtained and the analysis, check out the [report](https://gitlab.com/dm4562/machine-learning/blob/master/project2/dmehra6-analysis.pdf) in `project2` directory.

## Project 3 - Unsupervised Learning

This project was focused on using Unsupervised Learning to revisit some of the older problems to try and improve the results previously achieved.

2 clustering algorithms were used in this project:

1. k - Means Clustering
2. Expectation Maximization

The 4 Dimensionality reduction algorithms used in this project:

1. PCA
2. ICA
3. Randomized Projections
4. Variance Threshold Reduction

To learn more about the methodology used, results obtained and the analysis, check out the [report](https://gitlab.com/dm4562/machine-learning/blob/master/project3/dmehra6-analysis.pdf) in `project3` directory.

## Project 4 - MDPs and Reinforcement Learning

This project focuses on coming up with 2 interesting MDP problems and solving them using **Policy Iteration** and **Value Iteration**. Again, to learn more about the methodology, results and analysis, check out the [report](https://gitlab.com/dm4562/machine-learning/blob/master/project4/dmehra6-analysis.pdf) in `project4` directory.

