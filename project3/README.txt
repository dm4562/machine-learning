For rest of the algorithms python scripts were written to plot the 
metrics.

Interpreter: Python 3.6 and Python 2.7
Libraries:
- Numpy
- Scipy
- Scikit-learn
- Matplotlib
- Pandas

All the graphs are stored in the graphs directory and all the required outputs are 
in the data folder.


python3 em.py
python3 kmeans.py
python3 neural_net_clustering.py

python2 letter_dr.py
python2 letter_nn.py
python2 wine_dr.py
